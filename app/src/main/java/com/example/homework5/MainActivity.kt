package com.example.homework5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        loginbutton.setOnClickListener {
            if (usernameedittext.text.toString().isNotEmpty() && passwordedittext.text.toString()
                    .isNotEmpty()
            ){
                openProfile()
            }
        }
    }

    private fun openProfile() {
        val intent = Intent(this, MainProfile::class.java)
        startActivity(intent)
    }
}